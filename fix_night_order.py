import json
data = json.load(open('roles.json', 'r'))
all_ids = [role['id'] for role in data]

# categories
night_order_config = json.load(open('night_order.json', 'r'))
first_night = night_order_config['first_night']
other_nights = night_order_config['other_nights']

try:
    for role in data:
        role_id = role["id"]

        # assertions
        if role['firstNightReminder']:
            assert role_id in first_night, f'role "{role_id}" has a firstNightReminder, but is not in the first night list in night_order.json'
        else:
            assert role_id not in first_night, f'role "{role_id}" has no firstNightReminder, but is in the first night list in night_order.json'
        
        if role['otherNightReminder']:
            assert role_id in other_nights, f'role "{role_id}" has an otherNightReminder, but is not in the other night list in night_order.json'
        else:
            assert role_id not in other_nights, f'role "{role_id}" has no otherNightReminder, but is in the other night list in night_order.json'

        # first night
        try:
            i = first_night.index(role_id)
            role['firstNight'] = 10 + 10 * i
        except ValueError:
            role['firstNight'] = 0 

        # other nights
        try:
            i = other_nights.index(role_id)
            role['otherNight'] = 10 + 10 * i
        except ValueError:
            role['otherNight'] = 0 

    # save
    json.dump(data, open('roles.json', 'w'), indent=4)
except AssertionError as e:
    print(e)
    exit(1)

# dump results
first_night_in_order = sorted(
    [role for role in data if role['firstNight'] != 0],
    key=lambda role: role['firstNight'])
first_night_summary = '\n'.join(f"{role['firstNight']:4}  {role['name']:12} {role['firstNightReminder']}" for role in first_night_in_order)
print('First night:')
print(first_night_summary)

print()

other_nights_in_order = sorted(
    [role for role in data if role['otherNight'] != 0],
    key=lambda role: role['otherNight'])
other_nights_summary = '\n'.join(f"{role['otherNight']:4}  {role['name']:12} {role['otherNightReminder']}" for role in other_nights_in_order)
print('Other nights:')
print(other_nights_summary)