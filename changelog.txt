# changelog

## 2020/12/30 - revisions after game on 12/26

- Asclepius and Cassandra go earlier in the night, Argos later.
- Cassandra is now a townsfolk. No longer must be mad about the lie, and instead doesn't know which is the lie until the following night.
- Andromeda is now an outsider.
- Icarus's ability no longer kills
- Hermes's ability is now triggered by nominations from good players only
- Pandora's ability now goes at night, may kill her instead of cause a "bad thing", and tells you which demon is in the game.
- Argos now acts before Artemis
- Script name, author, and logo added