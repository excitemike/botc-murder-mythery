import json
data = json.load(open('roles.json', 'r'))
for role in data:
    role_id = role["id"]
    role['image'] = f'https://excitemike.com/botc_murder_mythery/img/{role_id}.png'
json.dump(data, open('roles.json', 'w'), indent=4)