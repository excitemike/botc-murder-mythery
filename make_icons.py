try:
    from PIL import Image
except ModuleNotFoundError:
    import subprocess
    subprocess.run("python -m pip install --upgrade pip")
    subprocess.run("python -m pip install --upgrade Pillow")
from PIL import Image, ImageFilter, ImageChops
import json
import os
import sys

src_images_dir = 'src_images'
dst_images_dir = 'dst_images'
colors = json.load(open('colors.json','r'))
positioning = json.load(open('positioning.json','r'))
positioning_left, positioning_top, positioning_right, positioning_bottom = positioning['usable_region']
final_width = positioning['full_width']
final_height = positioning['full_height']
texture_image_path = os.path.join(src_images_dir, 'token.d0b9dbdf.png')
border_scale = 0.0625
dropshadow_scale = 4
drop_shadow_offset_x = 0
drop_shadow_offset_y = 6
drop_shadow_opacity = 128
margin = max(
    drop_shadow_offset_x+dropshadow_scale,
    drop_shadow_offset_y+dropshadow_scale,
    border_scale
)
this_file_time = max(*map(os.path.getmtime, (sys.argv[0], 'positioning.json', 'colors.json')))

def color_image(im, r, g, b):
    _, _, _, a_channel = im.split()
    r_channel = a_channel.point(lambda alpha: alpha*r//255)
    g_channel = a_channel.point(lambda alpha: alpha*g//255)
    b_channel = a_channel.point(lambda alpha: alpha*b//255)
    return Image.merge(im.mode, (r_channel, g_channel, b_channel, a_channel))

def make_border_image(im):
    a_channel = (im.getchannel('A')
        .filter(ImageFilter.CONTOUR)
        .filter(ImageFilter.GaussianBlur(border_scale))
        .point(lambda x: 255 if x < 255 else 0)
        .filter(ImageFilter.GaussianBlur(1)))
    other_channels = ImageChops.constant(a_channel, 255)
    im = Image.merge(im.mode, (other_channels, other_channels, other_channels, a_channel))
    return im

def make_dropshadow_image(im):
    a_channel = (im.getchannel('A')
        .transform(im.size, Image.AFFINE, (1,0,-drop_shadow_offset_x,0,1,-drop_shadow_offset_y))
        .filter(ImageFilter.GaussianBlur(dropshadow_scale))
        .point(lambda x: drop_shadow_opacity * x // 255))
    other_channels = ImageChops.constant(a_channel, 0)
    im = Image.merge(im.mode, (other_channels, other_channels, other_channels, a_channel))
    return im

def get_bbox(im):
    left, top, right, bottom = im.getbbox()
    w = right - left
    h = bottom - top
    size = max(w,h)
    h_adjust = (size - w) // 2 + margin
    v_adjust = (size - h) // 2 + margin
    left -= h_adjust
    right += h_adjust
    top -= v_adjust
    bottom += v_adjust
    return left, top, right, bottom

roles = json.load(open('roles.json', 'r'))
for role in roles:
    src_file = os.path.join(src_images_dir, role['id'] + '.png')
    dst_file = os.path.join(dst_images_dir, role['id'] + '.png')
    if os.path.isfile(src_file):
        dst_file = src_file.replace(src_images_dir, dst_images_dir)
        dst_mtime = os.path.getmtime(dst_file) if os.path.exists(dst_file) else 0.0
        if not os.path.isfile(dst_file) or (this_file_time >= dst_mtime) or (os.path.getmtime(src_file) >= dst_mtime):
            print('creating icon for', role['id'])
            icon_im = Image.open(src_file)
            texture_im = Image.open(texture_image_path)
            color = colors[role['team']] if 'team' in role else (255, 255, 255)
            icon_im = color_image(icon_im, *color)
            crop_box = get_bbox(icon_im)
            #icon_im.alpha_composite(make_border_image(icon_im))
            #icon_im = Image.alpha_composite(make_dropshadow_image(icon_im), icon_im)
            icon_im = icon_im.crop(crop_box)
            resized_margin = int(margin * (positioning_right-positioning_left) / (crop_box[2] - crop_box[0]))
            icon_im = icon_im.resize((2*resized_margin+positioning_right-positioning_left, 2*resized_margin+positioning_bottom-positioning_top))
            fullsize_im = Image.new(icon_im.mode, (final_width, final_height), (0,0,0,0))
            fullsize_im.paste(icon_im, (positioning_left-resized_margin, positioning_top-resized_margin))
            fullsize_im = ImageChops.multiply(fullsize_im, texture_im)

            border_image = make_border_image(fullsize_im).crop((positioning_left, positioning_top, positioning_right, positioning_bottom))
            fullsize_im.alpha_composite(border_image, (positioning_left, positioning_top))
            fullsize_im = Image.alpha_composite(make_dropshadow_image(fullsize_im), fullsize_im)

            fullsize_im.save(dst_file)
        else:
            print(role['id'], 'up to date')
    else:
        print(f'missing source file "{src_file}"')
        exit(1)
