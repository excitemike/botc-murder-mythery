try:
    import paramiko
except ModuleNotFoundError:
    import subprocess
    subprocess.run("python -m pip install --upgrade pip")
    subprocess.run("python -m pip install --upgrade paramiko")
import paramiko
import json
import os
import random
login_info = json.load(open('ftppass.json', "r"))
host = login_info["host"]
port = login_info["port"]
user = login_info["user"]
passwd = login_info["passwd"]
images_dir = 'dst_images'
dst_dir = 'botc_murder_mythery'
local_files = ['roles.json']
roles = json.load(open('roles.json', 'r'))
upload_times = json.load(open('upload_times.json', 'r')) if os.path.exists('upload_times.json') else {}

def create_sftp():
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(host, port, user, passwd)
    sftp = ssh.open_sftp()
    sftp.sshclient = ssh
    return sftp

def find_files():
    to_upload = []
    # images
    for role in roles:
        role_id = role['id']
        fname = role_id + '.png'
        local_path = os.path.join(images_dir, fname)
        remote_path = '/'.join((dst_dir, 'img', fname))
        to_upload.append((local_path, remote_path))
    # other
    for local_path in local_files:
        remote_path = '/'.join((dst_dir, local_path))
        to_upload.append((local_path, remote_path))
    return to_upload
    
with create_sftp() as sftp:
    for (local_path, remote_path) in find_files():
        mtime = os.path.getmtime(local_path)
        if local_path not in upload_times or upload_times[local_path] < mtime:
            print('uploading', local_path, 'to', remote_path)
            sftp.put(local_path, remote_path)
            upload_times[local_path] = mtime
        else:
            print(local_path, 'up to date')

    print(f'\nrules.json url:\n\thttps://meyermik.startlogic.com/botc_murder_mythery/roles.json?{random.randrange(16**8):08x}')

    # save updated timestamps
    json.dump(upload_times, open('upload_times.json', 'w'), indent=4)